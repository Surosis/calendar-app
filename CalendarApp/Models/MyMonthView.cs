﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CalendarApp.Models
{
    public class MyMonthView
    {
        public int ID { get; set; }
        public List<List<DayOfMonth>> Weeks { get; set; }
        public DateTime Month { get; set; }
        public DateTime Year { get; set; }
    }

    public class DayOfMonth
    {
        public DateTime Date { get; set; }
        public List<NewEvent> Events { get; set; }
    }
}