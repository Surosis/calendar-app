﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CalendarApp.Models
{
    public class NewEvent
    {
        public int ID { get; set; }
        public string EventName { get; set; }
        public string FirstName { get; set; }
        public DateTime TimeofEvent { get; set; }
        public string Location { get; set; }
        public string AdditionalNotes { get; set; }
    }
}