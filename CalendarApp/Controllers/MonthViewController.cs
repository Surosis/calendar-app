﻿using CalendarApp.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CalendarApp.Controllers
{
    public class MonthViewController : Controller
    {
            private ApplicationDbContext _context;

            public MonthViewController()
            {
                _context = new ApplicationDbContext();
            }

            protected override void Dispose(bool disposing)
            {
                _context.Dispose();
                base.Dispose(disposing);
            }

            // GET: MonthView
            public ActionResult Index(DateTime? month = null)
        {
            if (month == null)
            {
                month = DateTime.Today;
            }
            month = month.Value.AddDays(1 - month.Value.Day);

            var dates = new List<DateTime>();
            dates.Add(month.Value);

            for (var day = month.Value.AddDays(1); day.Month == month.Value.Month; day = day.AddDays(1))
            {
                dates.Add(day);
            }

            var dayOfWeek = month.Value.DayOfWeek;

            var weeks = new List<List<DayOfMonth>>
            {
                new List<DayOfMonth>(),
            };

            var currentWeek = 0;
            foreach (var date in dates)
            {
                var week = weeks[currentWeek];
                var tomorrow = date.AddDays(1);
                week.Add(new DayOfMonth
                {
                    Date = date,
                    Events = _context.NewEvents
                        .Where(e => e.TimeofEvent >= date && e.TimeofEvent < tomorrow)
                        .ToList()
                });

                if (date.DayOfWeek == DayOfWeek.Saturday)
                {
                    currentWeek = currentWeek + 1;
                    weeks.Add(new List<DayOfMonth>());
                }
            }

            return View(new MyMonthView
            {
                Month = month.Value,
                Weeks = weeks,
            });
        }

        [HttpPost]
        public ActionResult New(NewEvent newEvent)
        {
            //Create a new event, add it and save it to the database
            if (newEvent != null)
            {
                _context.NewEvents.Add(newEvent);
                _context.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        public ActionResult Single(int ID)
        {
            //Fetch the single event from the database
            NewEvent MyEvent;
            MyEvent = _context.NewEvents.SingleOrDefault(e => e.ID == ID);
            if(MyEvent == null)
            {
                return HttpNotFound();
            }
            return View(MyEvent);

        }

        public ActionResult Delete(int ID)
        {
            //Delete event from the database forever
            _context.NewEvents.Remove(_context.NewEvents.Find(ID));
            _context.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int ID)
        {
            NewEvent newEvent = _context.NewEvents.Find(ID);
            return View(newEvent);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit([Bind(Include = "ID, EventName, FirstName, TimeofEvent, Location, AdditionalNotes")] NewEvent newEvent)
        {
            //Edit an event from the database through the edit button
            if (newEvent != null)
            {
                _context.Entry(newEvent).State = EntityState.Modified;
                _context.SaveChanges();
            }
            return RedirectToAction("Index");
        }
    }
}