namespace CalendarApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewEvent : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.NewEvents",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        EventName = c.String(),
                        FirstName = c.String(),
                        TimeofEvent = c.DateTime(nullable: false),
                        Location = c.String(),
                        AdditionalNotes = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.NewEvents");
        }
    }
}
